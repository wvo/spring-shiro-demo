package com.itaem.crazy.shirodemo.modules.shiro.dao;

import com.itaem.crazy.shirodemo.modules.shiro.entity.SysToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 同步数据库token映射器
 */

public interface SysTokenRepository extends JpaRepository<SysToken, Integer> {
    SysToken findByToken(String accessToken);

    SysToken findByUserId(Integer userId);
}
