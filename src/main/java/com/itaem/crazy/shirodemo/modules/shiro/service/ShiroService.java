package com.itaem.crazy.shirodemo.modules.shiro.service;


import com.itaem.crazy.shirodemo.modules.shiro.entity.SysToken;
import com.itaem.crazy.shirodemo.modules.shiro.entity.User;

import java.util.Map;

/**
 * 服务类
 */
public interface ShiroService {
     /**
      * Find user by username
      * @param username
      * @return
      */
     User findByUsername(String username);

     /**
      * create token by userId
      * @param userId
      * @return
      */
     Map<String,Object> createToken(Integer userId);

     /**
      * logout
      * @param userId
      */
     void logout(Integer userId);

     /**
      * find token by token
      * @param accessToken
      * @return
      */
     SysToken findByToken(String accessToken);

     /**
      * find user by userId
      * @param userId
      * @return
      */
     User findByUserId(Integer userId);

     /**
      * 管理员注册（插入一条数据）
      * @param user
      * @return
      */
     boolean insert(User user);




}
