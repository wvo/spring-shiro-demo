package com.itaem.crazy.shirodemo.modules.shiro.dao;

import com.itaem.crazy.shirodemo.modules.shiro.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 同步数据库权限映射器
 * 继承jpa
 */
public interface PermissionRepository extends JpaRepository<Permission, Integer> {
}
