package com.itaem.crazy.shirodemo.modules.shiro.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * 用户实体
 */
@Getter
@Setter
@Entity
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)  //【jpa】
    private Integer userId;

    private String username;

    private String password;

    //是否生效
    private int valid;

    //盐
    private String salt;

    //创建时间
    private Date createTime;

    //是否删除
    private int del;

    @ApiModelProperty(value = "角色对象",hidden = true)  //value:字段加注释;hidden:字段不显示在swagger-ui的实力输入框中
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "userId")},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "roleId")})
    private Set<Role> roles;

}
