package com.itaem.crazy.shirodemo.modules.shiro.dao;

import com.itaem.crazy.shirodemo.modules.shiro.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 同步数据库角色映射器
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
